# README #

This is a clone of [the page of Pinterest](https://business.pinterest.com/en)

# Pre-requisite #

- PHP
- MySQL

## MySQL setup ##

Default MySQL Connection:

- Host: localhost
- Username: root
- Password: root
- Database: frontend

Import the following code to MySQL as dummy data

```mysql
DROP TABLE IF EXISTS `Articles`;

CREATE TABLE `Articles` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `publication_date` date NOT NULL,
  `image_link` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `Articles` (`id`, `title`, `publication_date`, `image_link`)
VALUES
	(1,'From hygiene to hair health, Pinterest gets personal','2017-07-17','assets/img/articles/personal-care-blog-thumbnail-large.png'),
	(2,'New webinars to get you rolling on Pinterest','2017-07-11','assets/img/articles/SMB-webinar-blog1-thumbnail.jpg'),
	(3,'The retailer’s guide to customer retention on Pinterest','2017-07-05','assets/img/articles/retail-retention-thumbnail.png'),
	(4,'Time well spent at Cannes Lions 2017','2017-06-29','assets/img/articles/image-2.jpg'),
	(5,'Free benefits for businesses on Pinterest','2017-07-17','assets/img/articles/SMB-webinar-blog2-thumbnail.png'),
	(6,'July trend forecast: DIY slime, strawberry s’mores and summer’s “it” print','2017-07-10','assets/img/articles/pinsights-forecast-july-thumbnail-large.png'),
	(7,'Summer entertaining trends that are rising with the temps','2017-06-27','assets/img/articles/summer-entertainment-thumbnail.png'),
	(8,'The graduate’s guide to adulting','2017-06-29','assets/img/articles/BlogThumbnail-GradBook-560x904px.png');
```

# Development #

This webapp can be launched with PHP built-in server (if it is supported by the version of PHP you're using). To run the server, use command `php -S localhost:8000` in terminal while the working directory should be the folder of this repository.

# Missing functions #

- Mobile navbar
- Some animations