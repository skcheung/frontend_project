<?php
// MySQL Connection Variables
$servername = "localhost";
$username = "root";
$password = "root";

// MySQL Connection
$conn = mysqli_connect($servername, $username, $password);

// Check Connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
} else {
    // echo "Connected successfully";
}

// Create Database if not exists
$dbname = "frontend";
$sql = "CREATE DATABASE IF NOT EXISTS $dbname";
if (mysqli_query($conn, $sql)) {
    // echo "Success";
} else {
    // echo "Error creating database: " . mysqli_error($conn);
}

// Create table
$conn = mysqli_connect($servername, $username, $password, $dbname);
$sql = "CREATE TABLE Articles (
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
title TEXT NOT NULL,
publication_date date NOT NULL,
image_link TEXT NOT NULL
)";

if ($conn->query($sql) === TRUE) {
    // echo "Table Articles created successfully";
} else {
    // echo "Error creating table: " . $conn->error;
}

$sql = "INSERT INTO Articles (title, publication_date, image_link)
VALUES ('From hygiene to hair health, Pinterest gets personal', '2017-07-17', 'assets/img/articles/personal-care-blog-thumbnail-large.png');";
$sql .= "INSERT INTO Articles (title, publication_date, image_link)
VALUES ('New webinars to get you rolling on Pinterest', '2017-07-11', 'assets/img/articles/SMB-webinar-blog1-thumbnail.jpg');";
$sql .= "INSERT INTO Articles (title, publication_date, image_link)
VALUES ('The retailer’s guide to customer retention on Pinterest', '2017-07-05', 'assets/img/articles/retail-retention-thumbnail.png');";
$sql .= "INSERT INTO Articles (title, publication_date, image_link)
VALUES ('Time well spent at Cannes Lions 2017', '2017-06-29', 'assets/img/articles/image-2.jpg');";
$sql .= "INSERT INTO Articles (title, publication_date, image_link)
VALUES ('Free benefits for businesses on Pinterest', '2017-07-17', 'assets/img/articles/SMB-webinar-blog2-thumbnail.png');";
$sql .= "INSERT INTO Articles (title, publication_date, image_link)
VALUES ('July trend forecast: DIY slime, strawberry s’mores and summer’s “it” print', '2017-07-10', 'assets/img/articles/pinsights-forecast-july-thumbnail-large.png');";
$sql .= "INSERT INTO Articles (title, publication_date, image_link)
VALUES ('Summer entertaining trends that are rising with the temps', '2017-06-27', 'assets/img/articles/summer-entertainment-thumbnail.png');";
$sql .= "INSERT INTO Articles (title, publication_date, image_link)
VALUES ('The graduate’s guide to adulting', '2017-06-29', 'assets/img/articles/BlogThumbnail-GradBook-560x904px.png');";

if ($conn->multi_query($sql) === TRUE) {
    // echo "New records created successfully";
} else {
    // echo "Error: " . $sql . "<br>" . $conn->error;
}

?>