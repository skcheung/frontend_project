<?php

// Page Config
$page_title = "Pinterest for Business";

// Header
require_once('views/partials/header.php');
require_once('views/partials/navbar.php');

require_once('views/content.php');
?>


<?php

// Footer
require_once('views/partials/footer.php');
?>