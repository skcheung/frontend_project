<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title><?php echo $page_title; ?> | Pinterest for Business</title>
  <link rel="stylesheet" type="text/css" href="assets/css/main.css">
</head>
<body>