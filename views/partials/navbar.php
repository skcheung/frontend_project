<header>
  <nav>
    <div class="container">
      <div class="brand-logo">
        <a href="#"><img src="assets/img/logo.svg" alt=""> <span>Business</span></a>
      </div>
      <ul class="main-menu">
        <li>
          <a href="#" id="menu-item1">Getting started</a>
        </li>
        <li>
          <a href="#" id="menu-item2">Tools</a>
        </li>
        <li>
          <a href="#" id="menu-item3">Ads</a>
        </li>
        <li>
          <a href="#" id="menu-item4">Success stories</a>
        </li>
        <li>
          <a href="#" id="menu-item5">News</a>
        </li>
      </ul>
      <ul class="navbar-right">
        <li><a href="#" id="menu-item6">Create ad</a></li>
        <li><a href="#" id="menu-item7">Sign up</a></li>
      </ul>
      <a class="navbar-right menu-icon" href="#"><img src="assets/img/menu-alt-512.png" alt=""></a>
    </div><!-- .container -->
    <div class="container dropdown-content hide" id="getting-started">
      <div class="row">
        <div class="col-3">
          <ul>
            <li class="submenu-title">The basics</li>
            <li class="submenu-item"><a href="#">Why Pinterest?</a></li>
            <li class="submenu-item"><a href="#">Set up your account</a></li>
            <li class="submenu-item"><a href="#">Get your website ready</a></li>
            <li class="submenu-item"><a href="#">Our audience</a></li>
            <li class="submenu-item"><a href="#">How Pinterest works</a></li>
          </ul>
        </div>
        <div class="col-3">
          <ul>
            <li class="submenu-title">Reach your goals</li>
            <li class="submenu-item"><a href="#">Drive awareness</a></li>
            <li class="submenu-item"><a href="#">Increase traffic</a></li>
            <li class="submenu-item"><a href="#">Deliver an action</a></li>
            <li class="submenu-item"><a href="#">Boost in-store or online sales</a></li>
          </ul>
        </div>
        <div class="col-3">
          <ul>
            <li class="submenu-title">Best practices</li>
            <li class="submenu-item"><a href="#">Make great Pins</a></li>
            <li class="submenu-item"><a href="#">Resources</a></li>
            <li class="submenu-item"><a href="#">Brand guidelines</a></li>
          </ul>
        </div>
        <div class="col-3" style="position: relative; top: 12px;">
          <p class="submenu-title">Featured</p>
          <figure>
            <img src="assets/img/0-header-boost-traffic-01__menu_promo.jpg" alt="" class="img-responsive">
            <figcaption>
              <a href="#" class="">Boost traffic</a>
            </figcaption>
          </figure>
        </div>
      </div>
    </div><!-- .container -->
    <div class="container dropdown-content hide" id="tools">
      <div class="row">
        <div class="col-3">
          <ul>
            <li class="submenu-title">On Pinterest</li>
            <li class="submenu-item"><a href="#">Pin stats</a></li>
            <li class="submenu-item"><a href="#">Pinterest Analytics</a></li>
            <li class="submenu-item"><a href="#">Rich Pins</a></li>
            <li class="submenu-item"><a href="#">Buyable Pins</a></li>
            <li class="submenu-item"><a href="#">Shop the Lock</a></li>
          </ul>
        </div>
        <div class="col-5">
          <ul>
            <li class="submenu-title">For your site</li>
            <li class="submenu-item"><a href="#">Save button</a></li>
            <li class="submenu-item"><a href="#">Widget builder</a></li>
          </ul>
        </div>
        <div class="col-4" style="position: relative; top: 12px;">
          <p class="submenu-title">Featured</p>
          <figure>
            <img src="assets/img/0-header-save-button-01__menu_promo.jpg" alt="" class="img-responsive">
            <figcaption>
              <a href="#" class="">Save button</a>
            </figcaption>
          </figure>
        </div>
      </div>
    </div><!-- .container -->
    <div class="container dropdown-content hide" id="ads">
      <div class="row">
        <div class="col-3">
          <ul>
            <li class="submenu-title">The basics</li>
            <li class="submenu-item"><a href="#">Why Pinterest ads work</a></li>
            <li class="submenu-item"><a href="#">Formats</a></li>
            <li class="submenu-item"><a href="#">Targeting</a></li>
            <li class="submenu-item"><a href="#">Measurement</a></li>
          </ul>
        </div>
        <div class="col-4">
          <ul>
            <li class="submenu-title">Get help</li>
            <li class="submenu-item"><a href="#">Marketing Partners</a></li>
            <li class="submenu-item"><a href="#">Pin Collective</a></li>
            <li class="submenu-item"><a href="#">Promoted Pins support</a></li>
            <li class="submenu-item"><a href="#">Using Ads Manager</a></li>
            <li class="submenu-item"><a href="#">Pinterest Propel</a></li>
            <li class="submenu-item"><a href="#">Agencies</a></li>
          </ul>
        </div>
        <div class="col-5" style="position: relative; top: 12px;">
          <p class="submenu-title">Featured</p>
          <div class="submenu-flexbox">
            <figure>
              <img src="assets/img/marketstar-partner-growth-page-asset-1_0.png" alt="" class="img-responsive">
              <figcaption>
                <a href="#">Pinterest Propel</a>
              </figcaption>
            </figure>
            <figure>
              <img src="assets/img/0-header-why-ads-work-01__menu_promo.jpg" alt="" class="img-responsive">
              <figcaption>
                <a href="#">Why Pinterest ads work</a>
              </figcaption>
            </figure>
          </div>
        </div>
      </div>
    </div><!-- .container -->
    <div class="container dropdown-content hide" id="success-stories">
      <div class="row">
        <div class="col-3">
          <ul>
            <li class="submenu-title">Learn what works</li>
            <li class="submenu-item"><a href="#">All success stories</a></li>
          </ul>
        </div>
        <div class="col-9" style="position: relative; top: 12px;">
          <p class="submenu-title">Featured</p>
          <div class="submenu-flexbox">
            <figure>
              <img src="assets/img/land-of-nod-menu-promo.png" alt="" class="img-responsive">
              <figcaption>
                <a href="#">Land of Nod</a>
              </figcaption>
            </figure>
            <figure>
              <img src="assets/img/lowes.jpg" alt="" class="img-responsive">
              <figcaption>
                <a href="#">Lowes</a>
              </figcaption>
            </figure>
            <figure>
              <img src="assets/img/hunter-lady-promo.jpg" alt="" class="img-responsive">
              <figcaption>
                <a href="#">Hunter</a>
              </figcaption>
            </figure>
          </div>
        </div>
      </div>
    </div><!-- .container -->
    <div class="container dropdown-content hide" id="news">
      <div class="row">
        <div class="col-3">
          <ul>
            <li class="submenu-title">What's new</li>
            <li class="submenu-item"><a href="#">Blog</a></li>
            <li class="submenu-item"><a href="#">Insights</a></li>
          </ul>
        </div>
        <div class="col-9" style="position: relative; top: 12px;">
          <p class="submenu-title">Featured</p>
          <div class="submenu-flexbox">
            <figure>
              <img src="assets/img/SMB-webinar-blog2-menu-promo-image.png" alt="" class="img-responsive">
              <figcaption>
                <a href="#">Free benefits for businesses on Pinterest</a>
              </figcaption>
            </figure>
            <figure>
              <img src="assets/img/summer-entertainment-facebook.png" alt="" class="img-responsive">
              <figcaption>
                <a href="#">Summer entertaining trends that are rising with the temps</a>
              </figcaption>
            </figure>
            <figure>
              <img src="assets/img/mi-blog-facebook.jpg" alt="" class="img-responsive">
              <figcaption>
                <a href="#">New measurement solutions show how intent turns to action on Pinterest</a>
              </figcaption>
            </figure>
          </div>
        </div>
      </div>
    </div><!-- .container -->
  </nav>
</header>