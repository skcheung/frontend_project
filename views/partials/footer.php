<footer>
    <div class="container">
      <div class="row">
        <div class="col-6">
          <a href="#"><img src="assets/img/badge-wordmark.svg" alt="" style="width: 30%; margin-bottom: 25px; margin-top: 30px;"></a>
          <span class="select-toggle">
            <select name="" id="lang__select">
              <option value="en-US">English</option>
              <option value="en-UK">English UK</option>
              <option value="id">Bahasa Indonesia</option>
            </select>
          </span>
        </div>

        <div class="col-3">
          <ul>
            <li>About us</li>
            <li><a href="#">What's Pinterest</a></li>
            <li><a href="#">Our blog</a></li>
            <li><a href="#">Our Pinterest page</a></li>
            <li><a href="#">Engineering blog</a></li>
            <li><a href="#">Brand guidelines</a></li>
            <li><a href="#">Careers</a></li>
            <li><a href="#">Help Center</a></li>
          </ul>
        </div>
        <div class="col-3">
          <ul>
            <li>Our policies</li>
            <li><a href="#">Copyright & Trademark</a></li>
            <li><a href="#">Terms of service</a></li>
            <li><a href="#">Privacy & Cookies</a></li>
          </ul>
          <ul>
            <li>More info</li>
            <li><a href="#">For businesses</a></li>
            <li><a href="#">For developers</a></li>
            <li><a href="#">For press</a></li>
          </ul>
        </div>
      </div>
    </div>
  </footer>

  <script src="assets/js/main.js"></script>

</body>
</html>