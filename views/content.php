<div class="header__bg-img" style="background-image: url('assets/img/pinterest-biz-hero-main.jpg')">
    <span class="header__bg-img-overlay"></span>
    <div class="header__inner fade-in-down">
      <h1 class="header__text text__white">Get discovered. Increase consideration. Drive results.</h1>
      <p class="header__message text__white">People on Pinterest are open to all kinds of possibilities. Whether you want to <a href="/">drive awareness</a>, <a href="/">increase traffic</a>, <a href="/">deliver an action</a> or <a href="/">boost in-store or online sales</a>, Pinterest has got the solution for you.</p>
      <a class="btn-primary">Join as a business</a>
    </div>
    <div class="header__footer fade-in-down" onclick="location.href='/#headline'">
      <p>Learn more</p>
      <div class="arrow bounce"></div>
    </div>
  </div>

  <div class="container" id="headline">
    <div class="row">
      <div class="col-6">
        <h2 class="headline__title">Pinterest inspires people to act</h2>
      </div>
      <div class="col-6">
        <p class="headline__text">Every day, millions of people use Pinterest to find ideas for all parts of their lives. More than 75% of the ideas on Pinterest come from businesses like yours, which is why you’re so important to Pinners.</p>
        <a class="btn-primary">See how it works</a>
      </div>
    </div>
  </div>

  <div class="container">
    <div class="row" id="stat__grid">
      <div class="col-6">
        <figure>
          <div class="stat__numbers text__dark__green" id="people__stat">175m</div>
          <figcaption class="stat__caption">people</figcaption>
        </figure>
      </div>
      <div class="col-6">
        <figure>
          <div class="stat__numbers text__dark__cork" id="international__stat">50%+</div>
          <figcaption class="stat__caption">international</figcaption>
        </figure>
      </div>
      <div class="col-6">
        <figure>
          <div class="stat__numbers text__dark__pink" id="mobile__stat">80%</div>
          <figcaption class="stat__caption">on mobile</figcaption>
        </figure>
      </div>
      <div class="col-6">
        <figure>
          <div class="stat__numbers text__dark__blue" id="ideas__stat">100b</div>
          <figcaption class="stat__caption">ideas</figcaption>
        </figure>
      </div>
    </div>
  </div>

  <div class="container">
    <iframe width="853" height="480" src="https://www.youtube.com/embed/e60MWi6m9jI" frameborder="0" allowfullscreen></iframe>
  </div>

  <div class="container">
    <div class="row">
      <div class="col-9">
        <h2 class="headline__title" style="max-width: 75%;">Read business success stories</h2>
      </div>
      <div class="col-3">
        <a href="#" class="headline__link">See more</a>
      </div>
    </div>
    <div class="row" id="success__story__grid">
      <div class="col-3">
        <figure>
          <a href="#"><img src="assets/img/hillarys.jpg" alt="" class="img-responsive"></a>
          <figcaption>
            <h4>Hillarys</h4>
            <p>The interiors retailer saw a 6x increase in traffic over 12 months</p>
          </figcaption>
        </figure>
      </div>
      <div class="col-3">
        <figure>
          <a href="#"><img src="assets/img/tesco_ireland.jpg" alt="" class="img-responsive"></a>
          <figcaption>
            <h4>Tesco Ireland</h4>
            <p>Promoted Pins helped Tesco increase engagement</p>
          </figcaption>
        </figure>
      </div>
      <div class="col-3">
        <figure>
          <a href="#"><img src="assets/img/warner-brothers-thumbnail.png" alt="" class="img-responsive"></a>
          <figcaption>
            <h4>Legendary Entertainment/Warner Bros.</h4>
            <p>Promoted Video teased a blockbuster film</p>
          </figcaption>
        </figure>
      </div>
      <div class="col-3">
        <figure>
          <a href="#"><img src="assets/img/starz-thumbnail.jpg" alt="" class="img-responsive"></a>
          <figcaption>
            <h4>STARZ</h4>
            <p>Pinterest campaigns enticed people to try a new STARZ series</p>
          </figcaption>
        </figure>
      </div>
    </div>
  </div>

  <div class="container quote">
    <div class="row">
      <div class="col-6">
        <div class="quote__footer fade-in-down">
          <p>Kate Thurmes</p>
          <p>Artifact Uprising</p>
        </div>
      </div>
      <div class="col-6">
        <div class="quote__large fade-in-down">“We'd still be working in a basement if it weren't for Pinterest.”</div>
      </div>
    </div>
  </div>

  <div class="container">
    <div class="row">
      <div class="col-9">
        <h2 class="headline__title" style="max-width: 75%;">What's new</h2>
      </div>
      <div class="col-3">
        <a href="#" class="headline__link">See more</a>
      </div>
    </div>
    <div class="row">
      <?php

      $link = mysqli_connect("localhost", "root", "root") or die;

      mysqli_select_db($link, "frontend");

      $sql = "SELECT * FROM Articles";

      $result = mysqli_query($link, $sql);

      $total_records = mysqli_num_rows($result);

      ?>
      <?php for ($i=0; $i < $total_records; $i++) { $row = mysqli_fetch_assoc($result); ?>
      <div class="col-3 news-article">
        <img src="<?php echo $row[image_link]; ?>" alt="" class="img-responsive">
        <p><?php echo date("F j, Y", strtotime($row[publication_date])); ?></p>
        <h4><a href="#"><?php echo $row[title]; ?></a></h4>
      </div>
      <?php } ?>
    </div>
  </div><!-- .container -->

  <div class="container" id="cta">
    <div class="row" style="clear: both;">
      <div class="cta__title">
        <div>Get a business account</div>
      </div>
      <div>
        <a href="#" class="btn-primary">Sign up</a>
      </div>
    </div>
    <div class="row" style="clear: both;">
      <div class="cta__title">
        <div>Create Pinterest ads</div>
      </div>
      <div>
        <a href="#" class="btn-primary-alt">Get started</a>
      </div>
    </div>
  </div><!-- .container -->

  <div class="container" style="clear: both;" id="sources">
    <small>Sources: Pinterest analysis 2016</small>
  </div>