var dropdownContent = document.querySelectorAll('.dropdown-content');

// Reset all submenu to hide state
function reset() {
  for (var i = 0; i < dropdownContent.length; i++) {
    dropdownContent[i].classList.add('hide');
    dropdownContent[i].classList.remove('show');
  }
}

var menuItem1 = document.querySelector('#menu-item1');
var gettingStarted = document.querySelector('#getting-started');

menuItem1.addEventListener('mouseenter', function() {
  reset();
  gettingStarted.classList.remove('hide');
  gettingStarted.classList.add('show');
  gettingStarted.addEventListener('mouseleave', function() {
    this.classList.remove('show');
    this.classList.add('hide');
  });
});

var menuItem2 = document.querySelector('#menu-item2');
var tools = document.querySelector('#tools');

menuItem2.addEventListener('mouseenter', function() {
  reset();
  tools.classList.remove('hide');
  tools.classList.add('show');
  tools.addEventListener('mouseleave', function() {
    this.classList.remove('show');
    this.classList.add('hide');
  });
});

var menuItem3 = document.querySelector('#menu-item3');
var ads = document.querySelector('#ads');

menuItem3.addEventListener('mouseenter', function() {
  reset();
  ads.classList.remove('hide');
  ads.classList.add('show');
  ads.addEventListener('mouseleave', function() {
    this.classList.remove('show');
    this.classList.add('hide');
  });
});

var menuItem4 = document.querySelector('#menu-item4');
var successStories = document.querySelector('#success-stories');

menuItem4.addEventListener('mouseenter', function() {
  reset();
  successStories.classList.remove('hide');
  successStories.classList.add('show');
  successStories.addEventListener('mouseleave', function() {
    this.classList.remove('show');
    this.classList.add('hide');
  });
});

var menuItem5 = document.querySelector('#menu-item5');
var news = document.querySelector('#news');

menuItem5.addEventListener('mouseenter', function() {
  reset();
  news.classList.remove('hide');
  news.classList.add('show');
  news.addEventListener('mouseleave', function() {
    this.classList.remove('show');
    this.classList.add('hide');
  });
});

var navbar = document.querySelector('nav');
var headline = document.querySelector('#headline');
var numberAnimateExecuted = false;

window.addEventListener('scroll', function(e) {
  if (window.scrollY >= 10) {
    navbar.classList.add('navbar-white');
  } else {
    navbar.classList.remove('navbar-white');
  }
  if (window.scrollY >= headline.scrollTop && !numberAnimateExecuted) {
    numberAnimate(0, 175, people, 'm');
    numberAnimate(0, 50, international, '%+');
    numberAnimate(0, 80, mobile, '%');
    numberAnimate(0, 100, ideas, 'b');
    numberAnimateExecuted = true;
  }
});

var people = document.querySelector('#people__stat');
var international = document.querySelector('#international__stat');
var mobile = document.querySelector('#mobile__stat');
var ideas = document.querySelector('#ideas__stat');

function numberAnimate(number, target, elm, unit) {
  var interval = setInterval(function() {
    elm.innerHTML = number + unit;
    if (number >= target) {
      clearInterval(interval);
      return;
    }
    number++;
  }, 10);
}