<?php

$link = mysqli_connect("localhost", "root", "root") or die;

mysqli_select_db($link, "frontend");

$sql = "SELECT * FROM Articles";

$result = mysqli_query($link, $sql);

$total_records = mysqli_num_rows($result);

?>

<table border="1">
  <tr>
    <td>ID</td>
    <td>TITLE</td>
    <td>DATE</td>
    <td>IMAGE LINK</td>
  </tr>

  <?php for ($i=0; $i < $total_records; $i++) { $row = mysqli_fetch_assoc($result); ?>
  <tr>
    <td><?php echo $row[id]; ?></td>
    <td><?php echo $row[title]; ?></td>
    <td><?php echo $row[publication_date]; ?></td>
    <td><?php echo $row[image_link]; ?></td>
  </tr>
  <?php } ?>
</table>